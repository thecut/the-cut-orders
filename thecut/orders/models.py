# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.db import models
from thecut.orders.abstract_models import (
    AbstractOrder, AbstractOrderStatusLog, AbstractOrderItem)


# Concrete models

class Order(AbstractOrder):
    pass


class OrderStatusLog(AbstractOrderStatusLog):

    order = models.ForeignKey(Order, related_name='statuslog')


class OrderItem(AbstractOrderItem):

    order = models.ForeignKey(Order, related_name='items')
