# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.db.models.query import QuerySet


class OrderQuerySet(QuerySet):
    def related_to_model(self, *models):
        """
        Filters queryset by model referenced in source or target generic
        relation.
        :param model: any model
        :return: filtered :py:class:`thecut.orders.querysets.OrderQuerySet`
        """
        if not models:
            return self.none()

        content_type, models_pks = self._get_content_type_pks(models[0], models)

        return self.filter(
            Q(source_object_id__in=models_pks,
              source_content_type=content_type) |
            Q(target_object_id__in=models_pks,
              target_content_type=content_type))

    def model_is_target(self, *models):
        """
        Filters queryset by model referenced in target generic relation.
        Can take list of models.
        If models have a different ContentTypes raises ValueException.

        :param model: any model
        :return: filtered :py:class:`thecut.orders.querysets.OrderQuerySet`
        """
        if not models:
            return self.none()

        content_type, models_pks = self._get_content_type_pks(models[0], models)

        return self.filter(target_object_id__in=models_pks,
                           target_content_type=content_type)

    def model_is_source(self, *models):
        """
        Filters queryset by model referenced in source generic relation.
        :param model: any model
        :return: filtered :py:class:`thecut.orders.querysets.OrderQuerySet`
        """
        if not models:
            return self.none()

        content_type, models_pks = self._get_content_type_pks(models[0], models)

        return self.filter(source_object_id__in=models_pks,
                           source_content_type=content_type)

    @staticmethod
    def _get_content_type_pks(model, models):
        """
        Returns model content_type, list of models PKs.
        Also checks models are all of the same type.

        :param model: any model
        :param models: list of models
        :return: (content_type of the first model, list of model + models PKs)
        """
        content_type = ContentType.objects.get_for_model(model)
        for m in models:
            if content_type != ContentType.objects.get_for_model(m):
                raise ValueError("All the models should be of the same class")
        return content_type, [m.pk for m in list(models) + [model]]
