# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from thecut.orders.constants import status

ORDER_STATUS_CHOICES = (
    (status.NEW, 'New'),
    (status.PROCESSING, 'Processing'),
    (status.DISPATCHED, 'Dispatched'),
    (status.RECEIVED, 'Received'),
    (status.CLOSED, 'Closed'),
    (status.CANCELLED, 'Cancelled')
)