# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from model_utils.managers import PassThroughManager
from thecut.authorship.models import Authorship
from thecut.orders import querysets
from thecut.orders.choices import ORDER_STATUS_CHOICES
from thecut.orders.constants import status

# Abstract models


class AbstractOrder(Authorship):
    """
    Order moves from 'source' to 'target' something that is linked in 'Items'.
    """
    code = models.SlugField(max_length=24, db_index=True, editable=False)
    source_content_type = models.ForeignKey(ContentType, related_name='+')
    source_object_id = models.PositiveIntegerField()
    source = generic.GenericForeignKey('source_content_type', 
                                       'source_object_id')

    target_content_type = models.ForeignKey(ContentType, related_name='+',
                                            blank=True, null=True)
    target_object_id = models.PositiveIntegerField(blank=True, null=True)
    target = generic.GenericForeignKey('target_content_type', 
                                       'target_object_id')

    objects = PassThroughManager().for_queryset_class(
        querysets.OrderQuerySet)()

    class Meta(Authorship.Meta):
        ordering = ['-updated_at']
        abstract = True

    def is_new(self):
        return self.get_status().status == status.NEW

    def is_processing(self):
        return self.get_status().status == status.PROCESSING

    def is_dispatched(self):
        return self.get_status().status == status.DISPATCHED

    def is_closed(self):
        return self.get_status().status == status.CLOSED

    def is_cancelled(self):
        return self.get_status().status == status.CANCELLED

    def is_received(self):
        return self.get_status().status == status.RECEIVED


@python_2_unicode_compatible
class AbstractOrderStatusLog(Authorship):
    status = models.CharField(max_length=15, choices=ORDER_STATUS_CHOICES)

    class Meta(Authorship.Meta):
        ordering = ['-created_at']
        abstract = True

    def __str__(self):
        return '{0}'.format(self.get_status_display())


class AbstractOrderItem(models.Model):

    item_content_type = models.ForeignKey(ContentType)
    item_object_id = models.PositiveIntegerField()
    item = generic.GenericForeignKey('item_content_type', 'item_object_id')

    quantity = models.PositiveIntegerField()

    class Meta:
        abstract = True
