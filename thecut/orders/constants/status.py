# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


NEW = 'NEW'
CANCELLED = 'CANCELLED'
PROCESSING = 'PROCESSING'
DISPATCHED = 'DISPATCHED'
RECEIVED = 'RECEIVED'
CLOSED = 'CLOSED'
