The Cut Django App: Orders


=============
thecut.orders
=============

To install this application (whilst in the project's activated virtualenv)::
    pip install git+ssh://git@git.thecut.net.au/thecut-orders


Add the ``thecut.orders`` to the project's ``INSTALLED_APPS`` setting::

    INSTALLED_APPS = (
        ...
        'thecut.orders',
    )
